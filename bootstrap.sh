#!/bin/bash
# LEMP App server install
# install php apache mysql and 
directory="/home/vagrant/public_html"
echo 'installing LAMP stack ...'
yum -y install wget
yum -y install vim

if [ ! -f /etc/yum.repos.d/epel.repo ]; then
	wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
	rpm -Uvh epel-release-6*.rpm
	rm -f epel*

fi

if [ ! -f /etc/yum.repos.d/remi.repo ]; then
	wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
	rpm -Uvh remi-release-6*.rpm
	rm -f remi*
fi

yum -y install httpd php php-common php-devel php-gd php-imap php-ldap php-magpierss php-mbstring php-mysql php-odbc php-pdo php-pear php-pecl-apc php-pecl-memcache php-snmp php-soap php-tidy php-xml php-xmlrpc php-zts memcached perl-Cache-Memcached git 

chkconfig httpd on
service httpd start
echo 'configuring apache for selinux ....'
cat > /etc/httpd/conf.d/userdir.conf << EOL
<IfModule mod_userdir.c>
    #
    # UserDir is disabled by default since it can confirm the presence
    # of a username on the system (depending on home directory
    # permissions).
    #
    UserDir enabled vagrant

    #
    # To enable requests to /~user/ to serve the user's public_html
    # directory, remove the "UserDir disabled" line above, and uncomment
    # the following line instead:
    #
    UserDir public_html

</IfModule>

<Directory /home/*/public_html>
        Options Indexes Includes FollowSymLinks
        ## Apache 2.2 users use following ##
        AllowOverride All
        Allow from all
        Order deny,allow
</Directory>
EOL

ln -fs /vagrant $directory

echo 'getting wordpress ... '
cd $directory
wget http://wordpress.org/latest.tar.gz
tar zxf latest.tar.gz
cd wordpress
cp -rpf * ../
cd ../
rm -rf wordpress/
rm -f latest.tar.gz


chmod 711 /home/vagrant
chown vagrant:vagrant $directory
chmod 755 $directory
setsebool -P httpd_enable_homedirs true
chcon -R -t httpd_sys_content_t $directory

echo 'configuring apache for virtual hosts ....'

cat >> /etc/httpd/conf/httpd.conf << EOL
NameVirtualHost *:80
<VirtualHost *:80>
DocumentRoot "$directory"
<Directory $directory>
allow from all
Options +Indexes
</Directory>
</VirtualHost>
<VirtualHost *:80>
DocumentRoot $directory
ServerName vagrant.tester
<Directory $directory>
allow from all
Options +Indexes
</Directory>
</VirtualHost>
EOL

setenforce 0
echo 'SELINUX=disabled' > /etc/sysconfig/selinux

service httpd restart

if [ -f $directory/db.sql ]; then
echo 'importing db ... '
mysql -h localhost --user=root --password=h00th00t! < $directory/db.sql
fi