### How do I get set up? ###

install vagrant and virtual box

add the vagrant-omnibus plugin before vagrant up

    vagrant plugin install vagrant-omnibus   

### Conditions ###

* sql file for the database import should be named as **db.sql**

### Frequently used commands ###

to bring up the vm

    vagrant up

to destroy the vm

    vagrant destroy

to reload / rerun the provisioning scripts

    vagrant reload
    
    vagrant provision